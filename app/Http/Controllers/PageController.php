<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
    	return view('views_app.home');
    }

    public function cart(){
    	return view('views_app.shop.cart');
    }

     public function checkout(){
    	return view('views_app.shop.checkout');
    }

    public function shop()
    {
    	return view('views_app.products');
    }
     public function singleProduct()
    {
    	return view('views_app.shop.single-product');
    }
}
