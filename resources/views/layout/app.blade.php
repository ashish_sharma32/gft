<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        @include('partials.app.meta')
        @include('partials.app.styles')
        @include('partials.app.polyfills')
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-border-bottom bg-white shop-nav" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 pull-left">
                        <a class="logo-light" href="/"><img alt="" src="images/gft-logo.png" class="logo" /></a>
                        <a class="logo-dark" href="/"><img alt="" src="images/gft-logo.png" class="logo" /></a>
                    </div>
                    <div class="col-md-2 no-padding-left search-cart-header pull-right">
                        <div id="top-search">
                            <a href="" class="header-search-form"><i class="fa fa-search search-button"></i></a>
                        </div>
                        <form id="search-header" method="post" action="#" name="search-header" class="mfp-hide search-form-result">
                            <div class="search-form position-relative">
                                <button type="submit" class="fa fa-search close-search search-button"></button>
                                <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                            </div>
                        </form>
                        <div class="top-cart">
                            <a href="#" class="shopping-cart">
                                <i class="fa fa-shopping-cart"></i>
                                <!-- <div class="subtitle">(1) Items</div> -->
                            </a>
                  <!--           <div class="cart-content">
                                <ul class="cart-list">
                                    <li>
                                        <a title="Remove item" class="remove" href="#">×</a>
                                        <a href="#">
                                            <img width="90" height="90" alt="" src="images/shop-cart.jpg">Leather Craft
                                        </a>
                                        <span class="quantity">1 × <span class="amount">$160</span></span>
                                        <a href="#">Edit</a>
                                    </li>
                                </ul>
                                <p class="total">Subtotal: <span class="amount">$160</span></p>
                                <p class="buttons">
                                    <a href="shop-cart.html" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr">View Cart</a>
                                    <a href="shop-checkout.html" class="btn btn-very-small-white no-margin-bottom margin-seven no-margin-right pull-right">Checkout</a>
                                </p>
                            </div>   -->
                        </div>
                    </div>
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <div class="col-md-8 no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Buy Gift Cards</a></li>
                                <li><a href="#">Sell Gift Cards</a></li>
                                <li><a href="#">Contact</a></li>

<!--                                 <li><a href="#">Home</a>                      
                                    <ul id="collapse1" class="dropdown-menu mega-menu panel-collapse collapse mega-menu-full">
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Pre Made Homepage</li>
                                                <li><a href="index-2.html">Home main</a></li>
                                                <li><a href="home-fashion.html">Home fashion</a></li>
                                                <li><a href="home-architecture.html">Home architecture</a></li>
                                                <li><a href="home-spa.html">Home spa</a></li>
                                                <li><a href="home-agency.html">Home agency</a></li>
                                                <li><a href="home-restaurant.html">Home restaurant</a></li>
                                                <li><a href="home-travel-agency.html">Home travel agency</a></li>
                                                <li><a href="home-corporate.html">Home corporate</a></li>
                                                <li><a href="home-photography.html">Home photography</a></li>
                                                <li><a href="home-shop.html">Home shop</a></li>
                                                <li><a href="home-blog.html">Home blog</a></li>
                                                <li><a href="home-blog-grid.html">Home blog grid</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Other</li>
                                                <li><a href="home-masonry-portfolio.html">Home - masonry portfolio</a></li>
                                                <li><a href="home-background-slider.html">Home - background slider</a></li>
                                                <li><a href="home-full-screen-video.html">Home - full screen video</a></li>
                                                <li><a href="home-half-screen-video.html">Home - half screen video</a></li>
                                                <li><a href="home-text-rotator.html">Home - text rotator</a></li>
                                                <li><a href="home-countdown-timer.html">Home - coming soon</a></li>
                                                <li><a href="home-countdown-timer-animation.html">Home - coming soon (Animation) <span class="menu-new">new</span></a></li>
                                                <li><a href="home-countdown-timer-video.html">Home - coming soon (video)</a></li>
                                                <li><a href="home-full-width-image.html">Home - full width image</a></li>
                                                <li><a href="home-gradient-with-image.html">Home - gradient with image</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Parallax Image / Typography</li>
                                                <li><a href="home-parallax1.html">Parallax Image - option 1 </a></li>
                                                <li><a href="home-parallax2.html">Parallax Image - option 2</a></li>
                                                <li><a href="home-parallax3.html">Parallax Image - option 3 </a></li>
                                                <li><a href="home-parallax4.html">Parallax Image - option 4</a></li>
                                                <li><a href="home-parallax5.html">Parallax Image - option 5</a></li>
                                                <li><a href="home-parallax.html">Full parallax home page</a></li>
                                            </ul>
                                            <ul>
                                                <li class="dropdown-header">Carousel slider</li>
                                                <li><a href="home-slider-bootstrap.html">Bootstrap slider - full screen</a></li>
                                                <li><a href="home-slider-revolution1.html">Revolution slider - full screen</a></li>
                                                <li><a href="home-slider-revolution2.html">Revolution slider - half screen</a></li>
                                                <li><a href="home-slider-owl1.html">owl slider - full screen</a></li>
                                                <li><a href="home-slider-owl2.html">owl slider - half screen</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Creative Intro Pages</li>
                                                <li><a href="intro-fashion.html">Fashion Intro</a></li>
                                                <li><a href="intro-restaurant.html">Restaurant Intro</a></li>
                                                <li><a href="intro-travel.html">Travel Intro</a></li>
                                                <li><a href="intro-travel2.html">Travel Intro - V2</a></li>
                                                <li><a href="intro-agency.html">Agency Intro</a></li>
                                                <li><a href="intro-agency2.html">Agency Intro - V2</a></li>
                                                <li><a href="intro-product.html">Product Intro</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> -->
<!--                                 <li class="dropdown panel"><a href="#collapse4" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Men <i class="fa fa-angle-down"></i></a>
                                    <ul id="collapse4" class="dropdown-menu mega-menu panel-collapse collapse mega-menu-full">
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Footwear</li>
                                                <li><a href="shop-with-sidebar.html">Casual Shoes</a></li>
                                                <li><a href="shop-with-sidebar.html">Formal Shoes</a></li>
                                                <li><a href="shop-with-sidebar.html">Sports shoes</a></li>
                                                <li><a href="shop-with-sidebar.html">Flip Flops</a></li>
                                                <li><a href="shop-with-sidebar.html">Slippers</a></li>
                                                <li><a href="shop-with-sidebar.html">Sports Sandals</a></li>
                                                <li><a href="shop-with-sidebar.html">Party Shoes</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Clothing</li>
                                                <li><a href="shop-with-sidebar.html">Casual Shirts</a></li>
                                                <li><a href="shop-with-sidebar.html">T-Shirts</a></li>
                                                <li><a href="shop-with-sidebar.html">Collared Tees</a></li>
                                                <li><a href="shop-with-sidebar.html">Pants / Trousers</a></li>
                                                <li><a href="shop-with-sidebar.html">Ethnic Wear</a></li>
                                                <li><a href="shop-with-sidebar.html">Jeans</a></li>
                                                <li><a href="shop-with-sidebar.html">Sweamwear</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-3 banner">
                                            <ul>
                                                <li><a href="shop-with-sidebar.html"><img src="images/banner2.jpg" alt=""/></a></li>
                                            </ul>
                                        </li>    
                                        <li class="mega-menu-column col-sm-3 banner">
                                            <ul>
                                                <li><a href="shop-with-sidebar.html"><img src="images/banner1.jpg" alt=""/></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown panel"><a href="#collapse3" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Women<i class="fa fa-angle-down"></i></a>
                                    <ul id="collapse3" class="dropdown-menu mega-menu panel-collapse collapse mega-menu-full">
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Footwear</li>
                                                <li><a href="shop-with-sidebar.html">Casual Shoes</a></li>
                                                <li><a href="shop-with-sidebar.html">Formal Shoes</a></li>
                                                <li><a href="shop-with-sidebar.html">Sports shoes</a></li>
                                                <li><a href="shop-with-sidebar.html">Flip Flops</a></li>
                                                <li><a href="shop-with-sidebar.html">Slippers</a></li>
                                                <li><a href="shop-with-sidebar.html">Sports Sandals</a></li>
                                                <li><a href="shop-with-sidebar.html">Party Shoes</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-3">
                                            <ul>
                                                <li class="dropdown-header">Clothing</li>
                                                <li><a href="shop-with-sidebar.html">Casual Shirts</a></li>
                                                <li><a href="shop-with-sidebar.html">T-Shirts</a></li>
                                                <li><a href="shop-with-sidebar.html">Collared Tees</a></li>
                                                <li><a href="shop-with-sidebar.html">Pants / Trousers</a></li>
                                                <li><a href="shop-with-sidebar.html">Ethnic Wear</a></li>
                                                <li><a href="shop-with-sidebar.html">Jeans</a></li>
                                                <li><a href="shop-with-sidebar.html">Sweamwear</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-sm-6 banner">
                                            <ul>
                                                <li><a href="shop-with-sidebar.html"><img src="images/banner5.jpg" alt=""/></a></li>
                                                <li class="margin-five no-margin-bottom"><a href="shop-with-sidebar.html"><img src="images/banner6.jpg" alt=""/></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown panel"><a href="shop-with-sidebar.html">Accessories</a></li>
                                <li class="dropdown panel"><a href="shop-with-sidebar.html">Sale</a></li>
                                <li class="dropdown panel simple-dropdown"><a href="blog-right-sidebar.html" >Blog</a></li>    -->   
                            </ul>
                        </div>
                    </div>      
                </div>
            </div>
        </nav>
        @yield('page-content')
        <footer>
            <div class=" bg-white footer-top">
                <div class="container">
                    <div class="row margin-four">
                        <div class="col-md-4 col-sm-4 text-center"><i class="icon-phone small-icon black-text"></i><h6 class="black-text margin-two no-margin-bottom">123-456-7890</h6></div>
                        <div class="col-md-4 col-sm-4 text-center"><i class="icon-map-pin small-icon black-text"></i><h6 class="black-text margin-two no-margin-bottom">SF, United States</h6></div>
                        <div class="col-md-4 col-sm-4 text-center"><i class="icon-envelope small-icon black-text"></i><h6 class="margin-two no-margin-bottom"><a href="mailto:sales@gft.to" class="black-text">sales@gft.to</a></h6></div>
                    </div>
                </div>
            </div>
            <div class="container footer-middle">
                <div class="row">
                    <div class="col-md-3 col-sm-3 footer-link1 xs-display-none">
                        <h5>About Us</h5>
                        <p class="footer-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-4 footer-link2 col-md-offset-3">
                        <h5>Company</h5>
                        <ul>
                            <li><a href="#">About Company</a></li>
                            <li><a href="#">What We Do</a></li>
                            <li><a href="#">Careers</a></li>
                        </ul>
                    </div>
                   <!--  <div class="col-md-2 col-sm-3 col-xs-4  footer-link3">
                        <h5>Services</h5>
                        <ul>
                            <li><a href="services.html">Web Development</a></li>
                            <li><a href="services.html">Graphic Design</a></li>
                            <li><a href="services.html">Copywriting</a></li>
                            <li><a href="services.html">Online Marketing</a></li>
                        </ul>
                    </div> -->
              <!--       <div class="col-md-2 col-sm-3 col-xs-4  footer-link4">
                        <h5>Introduction</h5>
                        <ul>
                            <li><a href="team-members.html">Team Members</a></li>
                            <li><a href="testimonials.html">Testimonials</a></li>
                            <li><a href="our-clients.html">Our Clients</a></li>
                            <li><a href="careers.html">Careers With Us</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="wide-separator-line bg-mid-gray no-margin-lr margin-three no-margin-bottom"></div>
                <div class="row margin-four no-margin-bottom">
                    <div class="col-md-6 col-sm-12 sm-text-center sm-margin-bottom-four">
                        <ul class="list-inline footer-link text-uppercase">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Testimonials</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12 footer-social text-right sm-text-center">
                        <a target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a>
                        <a target="_blank" href="https://dribbble.com/"><i class="fa fa-dribbble"></i></a>
                        <a target="_blank" href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a>
                        <a target="_blank" href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="container-fluid bg-dark-gray footer-bottom">
                <div class="container">
                    <div class="row margin-three">
                        <div class="col-md-6 col-sm-6 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one">
                            &copy; 2017 GFT.TO | All Rights Reserved
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 footer-logo text-right xs-text-center">
                            <a href="#"><img src="images/logo-footer.png" alt=""/></a>
                        </div>      
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a> 
        </footer>  
        @include('partials.app.scripts')
    </body>
</html>