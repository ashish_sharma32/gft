@extends('layout.app')
@section('title') GFT.TO | Buy and Sell Gift Cards @stop
@section('page-content')
<section id="slider" class="no-padding content-top-margin"> 
            <div id="owl-demo" class="owl-carousel owl-theme owl-half-slider dark-pagination dark-pagination-without-next-prev-arrow">
                <div class="item owl-bg-img" style="background-image:url('images/banner.png');">
                    <div class="container position-relative">
                        <div class="slider-typography slider-typography-shop text-right">
                            <div class="slider-text-middle-main">
                                <div class="slider-text-middle padding-left-right-px animated fadeInUp">
                                    <span class="owl-title text-right black-text xs-margin-bottom-seven">BUY, SELL AND REDEEM GIFT CARDS</span>
                                    <a href="#" class="highlight-button-dark btn margin-four">Browse Cards</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">

                    <div class="col-md-4 col-sm-4">

                        <div class="home-product text-center position-relative overflow-hidden margin-ten no-margin-top">
                            <a href="#"><img src="images/card.png" alt=""/></a>
                            <span class="product-name text-uppercase"><a href="#">Amazon Cards</a></span>
                            <span class="price black-text">$10-1000</span>
                            <div class="quick-buy">
                                <div class="product-share">
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist" data-wow-duration="300ms"><i class="fa fa-heart-o"></i></a>
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare" data-wow-duration="600ms"><i class="fa fa-refresh"></i></a>
                                    <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart" data-wow-duration="900ms"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="home-product text-center position-relative overflow-hidden margin-ten no-margin-bottom">
                            <a href=""><img src="images/card2.jpg" alt=""/></a>
                            <span class="product-name text-uppercase"><a href="">Best Buy</a></span>
                            <span class="price black-text">$10-1000</span>
                            <div class="quick-buy">
                                <div class="product-share">
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                    <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="col-md-4 col-sm-4 exclusive-style no-padding xs-margin-top-ten">
                        <div id="owl-demo-small" class="owl-carousel owl-theme owl-half-slider dark-pagination dark-pagination-without-next-prev-arrow">
                            <div class="item text-center"><a href=""><img src="images/card.png" alt=""/></a></div> 
                            <div class="item text-center"><a href=""><img src="images/card2.jpg" alt=""/></a></div> 
                            <div class="item text-center"><a href=""><img src="images/card3.png" alt=""/></a></div> 
                            <div class="item text-center"><a href=""><img src="images/card4.png" alt=""/></a></div> 
                        </div>
                        <div class="exclusive-style-text text-center">
                            <!-- <p class="text-med font-weight-600 black-text text-uppercase letter-spacing-2">What To Buy Now</p>
                            <p class="sm-display-none">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.</p> -->
                            <a class="highlight-button-dark btn btn-small no-margin-right no-margin-bottom margin-three" href="#">Discover all cards</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="home-product text-center position-relative overflow-hidden margin-ten no-margin-top">
                            <a href=""><img src="images/card3.png" alt=""/></a>
                            <span class="product-name text-uppercase"><a href="">Walmart</a></span>
                            <span class="price black-text">$10-1000</span>
                            <div class="quick-buy">
                                <div class="product-share">
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                    <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="home-product text-center position-relative overflow-hidden margin-ten no-margin-bottom">
                            <a href=""><img src="images/card4.png" alt=""/></a>
                            <span class="product-name text-uppercase"><a href="">Starbucks</a></span>
                            <span class="price black-text">$10-1000</span>
                            <div class="quick-buy">
                                <div class="product-share">
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                    <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>
            </div>
        </section>
<!--         <section class="no-padding">
            <div class="container-fluid bg-dark-gray">
                <div class="row">
                    <div class="col-md-4 col-sm-12 shop-newsletter-main">
                        <div class="shop-newsletter text-center">
                            <i class="icon-envelope medium-icon white-text margin-five no-margin-top"></i>
                            <p class="text-med text-uppercase letter-spacing-2 light-gray-text no-margin lg-display-block lg-margin-bottom-three">Subscribe for newsletter</p>
                            <p class="title-large font-weight-700 text-uppercase letter-spacing-2 white-text lg-display-none">Stay in touch </p>
                            <input type="text" name="name" placeholder="Enter Your Email" class="no-margin">
                            <a class="btn-small-white-background btn btn-small margin-five no-margin-right no-margin-bottom" href="#">Subscribe Now!</a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
<!--         <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="section-title">New arrivals this month</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="home-product text-center position-relative overflow-hidden margin-ten no-margin-top">
                            <a href="l"><img src="images/card.png" alt=""/></a>
                            <span class="product-name text-uppercase"><a href="">Amazon Gift Cards</a></span>
                            <span class="price black-text">$10-1000</span>
                            <div class="quick-buy">
                                <div class="product-share">
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                    <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                    <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
<!--         <section class="bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="section-title">Blog</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 blog-listing no-margin-bottom wow fadeIn xs-margin-bottom-seven" data-wow-duration="600ms">
                        <div class="blog-image"><a href=""><img src="images/blog-post31.jpg" alt=""/></a></div>
                        <div class="blog-details">
                            <div class="blog-date">Posted by <a href="">Paul Scrivens</a> | 02 January 2015</div>
                            <div class="blog-title"><a href="">Our latest collection review</a></div>
                            <div class="separator-line bg-black no-margin-lr no-margin-bottom"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 blog-listing no-margin-bottom wow fadeIn xs-margin-bottom-seven" data-wow-duration="600ms">
                        <div class="blog-image"><a href=""><img src="images/blog-post32.jpg" alt=""/></a></div>
                        <div class="blog-details">
                            <div class="blog-date">Posted by <a href="">Nathan Ford</a> | 01 January 2015</div>
                            <div class="blog-title"><a href="">Winter sale so hurry up!</a></div>
                            <div class="separator-line bg-black no-margin-lr no-margin-bottom"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 blog-listing no-margin-bottom wow fadeIn" data-wow-duration="600ms">
                        <div class="blog-image"><a href=""><img src="images/blog-post33.jpg" alt=""/></a></div>
                        <div class="blog-details">
                            <div class="blog-date">Posted by <a href="">Aarron Walter</a> | 01 January 2015</div>
                            <div class="blog-title"><a href="blog-single-right-sidebar.html">Discount 20% sale so don't late</a></div>
                            <div class="separator-line bg-black no-margin-lr no-margin-bottom"></div>
                        </div>
                    </div>  
                </div>
            </div>
        </section> -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="section-title">Featured Cards</h3>
                    </div>
                </div>
                <div class="row">
                    <div id="shop-products" class="owl-carousel owl-theme dark-pagination owl-no-pagination owl-prev-next-simple">
                        <div class="item">
                            <div class="home-product text-center position-relative overflow-hidden">
                                <a href=""><img src="images/card.png" alt=""/></a>
                                <span class="product-name text-uppercase"><a href="">Amazon Gift Cards</a></span>
                                <span class="price black-text">$10-1000</span>
                                <div class="quick-buy">
                                    <div class="product-share">
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                        <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="home-product text-center position-relative overflow-hidden">
                                <a href=""><img src="images/card2.jpg" alt=""/></a>
                                <span class="product-name text-uppercase"><a href="">Best Buy Gift Cards</a></span>
                                <span class="price black-text">$10-1000</span>
                                <div class="quick-buy">
                                    <div class="product-share">
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                        <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="home-product text-center position-relative overflow-hidden">
                                <a href=""><img src="images/card3.png" alt=""/></a>
                                <span class="product-name text-uppercase"><a href="">Walmart Gift Cards</a></span>
                                <span class="price black-text">$10-1000</span>
                                <div class="quick-buy">
                                    <div class="product-share">
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                        <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="home-product text-center position-relative overflow-hidden">
                                <a href=""><img src="images/card4.png" alt=""/></a>
                                <span class="product-name text-uppercase"><a href="">Starbucks Gift Cards</a></span>
                                <span class="price black-text">$10-1000</span>
                                <div class="quick-buy">
                                    <div class="product-share">
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                        <a href="#" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Compare"><i class="fa fa-refresh"></i></a>
                                        <a href="" class="highlight-button-dark btn btn-small no-margin-right quick-buy-btn" title="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-block text-center">
                        <div class="text-block-inner">
                            <p class="text-large text-uppercase no-margin-bottom">Some Offer</p>
                            <p class="title-small text-uppercase font-weight-600 black-text letter-spacing-1">10% Off</p>
                            <a class="highlight-button btn btn-small no-margin" href="#">View Offer</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-block text-center">
                        <div class="text-block-inner">
                            <p class="text-large text-uppercase no-margin-bottom">Some Offer</p>
                            <p class="title-small text-uppercase font-weight-600 black-text letter-spacing-1">10% Off</p>
                            <a class="highlight-button btn btn-small no-margin" href="#">View Offer</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-block text-center">
                        <div class="text-block-inner">
                            <p class="text-large text-uppercase no-margin-bottom">Some Offer</p>
                            <p class="title-small text-uppercase font-weight-600 black-text letter-spacing-1">10% Off</p>
                            <a class="highlight-button btn btn-small no-margin" href="#">View Offer</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-block text-center">
                        <div class="text-block-inner">
                            <p class="text-large text-uppercase no-margin-bottom">Some Offer</p>
                            <p class="title-small text-uppercase font-weight-600 black-text letter-spacing-1">10% Off</p>
                            <a class="highlight-button btn btn-small no-margin" href="#">View Offer</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div id="owl-demo-brand" class="owl-carousel owl-theme dark-pagination owl-no-pagination owl-prev-next-simple">
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                        <div class="item"><a href=""><img src="images/brand-logo.jpg" alt=""/></a></div>
                    </div>  
                </div>
            </div>
        </section>
@stop