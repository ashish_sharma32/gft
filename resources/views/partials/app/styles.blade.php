{!! Html::style('css/animate.css') !!}
{!! Html::style('css/bootstrap.css') !!}
{!! Html::style('css/et-line-icons.css') !!}
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
{!! Html::style('css/extralayers.css') !!}
{!! Html::style('css/settings.css') !!}
{!! Html::style('css/magnific-popup.css') !!}
{!! Html::style('css/owl.carousel.css') !!}
{!! Html::style('css/owl.transitions.css') !!}
{!! Html::style('css/full-slider.css') !!}
{!! Html::style('css/text-effect.css') !!}
{!! Html::style('css/menu-hamburger.css') !!}
{!! Html::style('css/style.css') !!}
{!! Html::style('css/responsive.css') !!}